import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
/*
 * Created by JFormDesigner on Thu Feb 19 23:45:01 GMT-08:00 2015
 */



/**
 * @author kalissa phelps
 */
public class eDung extends JFrame {
	public eDung() {
		initComponents();
	}

	private void startActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void saveActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void loadActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void formStateChanged(ChangeEvent e) {
		if(form.isSelected()) {
			username.setEnabled(true);
		}else{
			username.setEnabled(false);
		}
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - kalissa phelps
		tabbedPane1 = new JTabbedPane();
		panel1 = new JPanel();
		label2 = new JLabel();
		form = new JRadioButton();
		join = new JRadioButton();
		username = new JTextField();
		label3 = new JLabel();
		ancient = new JRadioButton();
		normal = new JRadioButton();
		attack = new JRadioButton();
		leech = new JRadioButton();
		panel2 = new JPanel();
		label6 = new JLabel();
		ancient2 = new JRadioButton();
		normal2 = new JRadioButton();
		comboBox1 = new JComboBox<>();
		panel3 = new JPanel();
		helmSlot = new JTextField();
		textField2 = new JTextField();
		plateSlot = new JTextField();
		textField6 = new JTextField();
		wepSlot = new JTextField();
		textField8 = new JTextField();
		textField9 = new JTextField();
		shieldSlot = new JTextField();
		textField11 = new JTextField();
		legSlot = new JTextField();
		textField13 = new JTextField();
		bootSlot = new JTextField();
		button4 = new JButton();
		button5 = new JButton();
		label4 = new JLabel();
		label5 = new JLabel();
		button3 = new JButton();

		//======== this ========
		setTitle("eDungeon - Ikov Dungeoneering");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		//======== tabbedPane1 ========
		{

			//======== panel1 ========
			{

				// JFormDesigner evaluation mark
				panel1.setBorder(new javax.swing.border.CompoundBorder(
					new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
						"JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
						java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

				panel1.setLayout(null);

				//---- label2 ----
				label2.setText("Reserved for logo");
				label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
				label2.setHorizontalAlignment(SwingConstants.CENTER);
				panel1.add(label2);
				label2.setBounds(25, 10, 395, 80);

				//---- form ----
				form.setText("Form Party");
				form.addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						formStateChanged(e);
					}
				});
				panel1.add(form);
				form.setBounds(new Rectangle(new Point(180, 140), form.getPreferredSize()));

				//---- join ----
				join.setText("Join Party");
				panel1.add(join);
				join.setBounds(180, 100, 79, 23);

				//---- username ----
				username.setEnabled(false);
				panel1.add(username);
				username.setBounds(150, 220, 150, username.getPreferredSize().height);

				//---- label3 ----
				label3.setText("Leader's Username");
				label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
				panel1.add(label3);
				label3.setBounds(new Rectangle(new Point(170, 195), label3.getPreferredSize()));

				//---- ancient ----
				ancient.setText("Ancient Prayer");
				panel1.add(ancient);
				ancient.setBounds(new Rectangle(new Point(65, 255), ancient.getPreferredSize()));

				//---- normal ----
				normal.setText("Normal Prayer");
				panel1.add(normal);
				normal.setBounds(new Rectangle(new Point(265, 255), normal.getPreferredSize()));

				//---- attack ----
				attack.setText("Attack");
				panel1.add(attack);
				attack.setBounds(new Rectangle(new Point(85, 280), attack.getPreferredSize()));

				//---- leech ----
				leech.setText("Leech");
				panel1.add(leech);
				leech.setBounds(new Rectangle(new Point(290, 280), leech.getPreferredSize()));

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel1.getComponentCount(); i++) {
						Rectangle bounds = panel1.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel1.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel1.setMinimumSize(preferredSize);
					panel1.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Team", panel1);

			//======== panel2 ========
			{
				panel2.setLayout(null);

				//---- label6 ----
				label6.setText("Reserved for logo");
				label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
				label6.setHorizontalAlignment(SwingConstants.CENTER);
				panel2.add(label6);
				label6.setBounds(25, 10, 395, 80);

				//---- ancient2 ----
				ancient2.setText("Ancient Prayer");
				panel2.add(ancient2);
				ancient2.setBounds(65, 260, 97, 23);

				//---- normal2 ----
				normal2.setText("Normal Prayer");
				panel2.add(normal2);
				normal2.setBounds(275, 260, 93, 23);

				//---- comboBox1 ----
				comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
					"Floor 3"
				}));
				panel2.add(comboBox1);
				comboBox1.setBounds(new Rectangle(new Point(185, 145), comboBox1.getPreferredSize()));

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel2.getComponentCount(); i++) {
						Rectangle bounds = panel2.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel2.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel2.setMinimumSize(preferredSize);
					panel2.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Solo", panel2);

			//======== panel3 ========
			{
				panel3.setLayout(null);

				//---- helmSlot ----
				helmSlot.setHorizontalAlignment(SwingConstants.CENTER);
				helmSlot.setText("0");
				panel3.add(helmSlot);
				helmSlot.setBounds(175, 45, 95, helmSlot.getPreferredSize().height);

				//---- textField2 ----
				textField2.setText("Helmet ID");
				textField2.setEditable(false);
				textField2.setFont(textField2.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField2.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField2);
				textField2.setBounds(180, 10, 80, 30);

				//---- plateSlot ----
				plateSlot.setHorizontalAlignment(SwingConstants.CENTER);
				plateSlot.setText("0");
				panel3.add(plateSlot);
				plateSlot.setBounds(175, 125, 100, 20);

				//---- textField6 ----
				textField6.setText("PlateBody ID");
				textField6.setEditable(false);
				textField6.setFont(textField6.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField6.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField6);
				textField6.setBounds(180, 90, 85, 30);

				//---- wepSlot ----
				wepSlot.setHorizontalAlignment(SwingConstants.CENTER);
				wepSlot.setText("0");
				panel3.add(wepSlot);
				wepSlot.setBounds(50, 125, 100, 20);

				//---- textField8 ----
				textField8.setText("Weapon ID");
				textField8.setEditable(false);
				textField8.setFont(textField8.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField8.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField8);
				textField8.setBounds(55, 90, 85, 30);

				//---- textField9 ----
				textField9.setText("Shield ID");
				textField9.setEditable(false);
				textField9.setFont(textField9.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField9.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField9);
				textField9.setBounds(305, 90, 85, 30);

				//---- shieldSlot ----
				shieldSlot.setHorizontalAlignment(SwingConstants.CENTER);
				shieldSlot.setText("0");
				panel3.add(shieldSlot);
				shieldSlot.setBounds(300, 125, 100, 20);

				//---- textField11 ----
				textField11.setText("Legs ID");
				textField11.setEditable(false);
				textField11.setFont(textField11.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField11.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField11);
				textField11.setBounds(180, 170, 85, 30);

				//---- legSlot ----
				legSlot.setHorizontalAlignment(SwingConstants.CENTER);
				legSlot.setText("0");
				panel3.add(legSlot);
				legSlot.setBounds(175, 205, 100, 20);

				//---- textField13 ----
				textField13.setText("Boots ID");
				textField13.setEditable(false);
				textField13.setFont(textField13.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField13.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField13);
				textField13.setBounds(180, 240, 85, 30);

				//---- bootSlot ----
				bootSlot.setHorizontalAlignment(SwingConstants.CENTER);
				bootSlot.setText("0");
				panel3.add(bootSlot);
				bootSlot.setBounds(175, 275, 100, 20);

				//---- button4 ----
				button4.setText("Save Equipment");
				button4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						saveActionPerformed(e);
					}
				});
				panel3.add(button4);
				button4.setBounds(new Rectangle(new Point(25, 275), button4.getPreferredSize()));

				//---- button5 ----
				button5.setText("Load Equipment");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						loadActionPerformed(e);
					}
				});
				panel3.add(button5);
				button5.setBounds(320, 275, 109, 23);

				//---- label4 ----
				label4.setText("Leave \"0\" if empty");
				label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
				panel3.add(label4);
				label4.setBounds(new Rectangle(new Point(340, 5), label4.getPreferredSize()));

				//---- label5 ----
				label5.setText("Leave \"0\" if empty");
				label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
				panel3.add(label5);
				label5.setBounds(10, 5, 106, 14);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel3.getComponentCount(); i++) {
						Rectangle bounds = panel3.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel3.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel3.setMinimumSize(preferredSize);
					panel3.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Equipment", panel3);
		}
		contentPane.add(tabbedPane1);
		tabbedPane1.setBounds(0, 0, 460, 335);

		//---- button3 ----
		button3.setText("Start eDungeon");
		button3.setFont(button3.getFont().deriveFont(button3.getFont().getStyle() | Font.BOLD));
		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startActionPerformed(e);
			}
		});
		contentPane.add(button3);
		button3.setBounds(0, 335, 460, 36);

		{ // compute preferred size
			Dimension preferredSize = new Dimension();
			for(int i = 0; i < contentPane.getComponentCount(); i++) {
				Rectangle bounds = contentPane.getComponent(i).getBounds();
				preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
				preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
			}
			Insets insets = contentPane.getInsets();
			preferredSize.width += insets.right;
			preferredSize.height += insets.bottom;
			contentPane.setMinimumSize(preferredSize);
			contentPane.setPreferredSize(preferredSize);
		}
		pack();
		setLocationRelativeTo(getOwner());

		//---- buttonGroup1 ----
		ButtonGroup buttonGroup1 = new ButtonGroup();
		buttonGroup1.add(form);
		buttonGroup1.add(join);

		//---- buttonGroup2 ----
		ButtonGroup buttonGroup2 = new ButtonGroup();
		buttonGroup2.add(ancient);
		buttonGroup2.add(normal);

		//---- buttonGroup3 ----
		ButtonGroup buttonGroup3 = new ButtonGroup();
		buttonGroup3.add(attack);
		buttonGroup3.add(leech);

		//---- buttonGroup4 ----
		ButtonGroup buttonGroup4 = new ButtonGroup();
		buttonGroup4.add(ancient2);
		buttonGroup4.add(normal2);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - kalissa phelps
	private JTabbedPane tabbedPane1;
	private JPanel panel1;
	private JLabel label2;
	private JRadioButton form;
	private JRadioButton join;
	private JTextField username;
	private JLabel label3;
	private JRadioButton ancient;
	private JRadioButton normal;
	private JRadioButton attack;
	private JRadioButton leech;
	private JPanel panel2;
	private JLabel label6;
	private JRadioButton ancient2;
	private JRadioButton normal2;
	private JComboBox<String> comboBox1;
	private JPanel panel3;
	private JTextField helmSlot;
	private JTextField textField2;
	private JTextField plateSlot;
	private JTextField textField6;
	private JTextField wepSlot;
	private JTextField textField8;
	private JTextField textField9;
	private JTextField shieldSlot;
	private JTextField textField11;
	private JTextField legSlot;
	private JTextField textField13;
	private JTextField bootSlot;
	private JButton button4;
	private JButton button5;
	private JLabel label4;
	private JLabel label5;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
