package org.ethan.dungeoneering.edungeon.api;

import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Tile;

public class eArea
{
 Tile topLeftTile;
 Tile botRightTile;
  
  public eArea(Tile topLeftTile, Tile botRightTile)
  {
    this.topLeftTile = topLeftTile;
    this.botRightTile = botRightTile;
  }
  
  public boolean inTheZone()
  {
    if ((Players.getMyPlayer().getLocation().getX() > topLeftTile.getX()) && 
      (Players.getMyPlayer().getLocation().getY() < topLeftTile.getY()) && 
      (Players.getMyPlayer().getLocation().getX() < botRightTile.getX()) && 
      (Players.getMyPlayer().getLocation().getY() > botRightTile.getY())) {
      return true;
    }
    return false;
  }
  public boolean npcInTheZone(int x, int y)
  { 
    if ((x > topLeftTile.getX()) && 
      (y < topLeftTile.getY()) && 
      (x < botRightTile.getX()) && 
      (y > botRightTile.getY())) {
      return true;
    }
    return false;
  }
}
