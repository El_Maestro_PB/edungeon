package org.ethan.dungeoneering.edungeon.api;

import org.parabot.environment.api.utils.Time;
import org.rev317.min.api.methods.Menu;

 
public enum ePrayer {
 
        THICK_SKIN(5609, 83), BURST_OF_STRENGTH(5610, 84), CLARITY_OF_THOUGHT(5611,
                        85), SHARP_EYE(19182, 700), MYSTIC_WILL(19814, 701), ROCK_SKIN(
                        5612, 86), SUPERHUMAN_STRENGTH(5613, 87), IMPROVED_REFLEXES(5614,
                        88), RAPID_RESTORE(5615, 89), RAPID_HEAL(5616, 90), PROTECT_ITEMS(
                        5617, 91), HAWK_EYE(19816, 702), MYSTIC_LORE(19818, 703), STEEL_SKIN(
                        5618, 92), ULTIMATE_STRENGTH(5619, 93), INCREDIBLE_REFLEXES(5620,
                        94), PROTECT_FROM_SUMMONING(23105, 708), PROTECT_FROM_MAGIC(5621,
                        96), PROTECT_FROM_MISSILES(5622, 96), PROTECT_FROM_MELEE(5623, 95), EAGLE_EYE(
                        19821, 704), MYSTIC_MIGHT(19823, 705), RETRIBUTION(683, 98), REDEMPTION(
                        684, 99), SMITE(685, 100), RAPID_RENEWAL(23109, 709), PIETY(19827,
                        707), CHIVALRY(19825, 706), RIGOUR(23113, 710), AUGURY(23116, 711), CURSE_PROTECT_ITEM(
                        21357, 724), SAP_WARRIOR(21359, 725), SAP_RANGER(21361, 726), SAP_MAGE(
                        21363, 727), SAP_SPIRIT(21365, 728), BERSKER(21367, 729), DEFLECT_SUMMONING(
                        22521, 730), DEFLECT_MAGIC(22517, 617), DEFLECT_RANGE(21373, 732), DEFLECT_MELEE(
                        		22521, 619), LEECH_ATTACK(21377, 734), LEECH_RANGED(21379, 735), LEECH_MAGIC(
                        21381, 736), LEECH_DEFENCE(21383, 737), LEECH_STRENGTH(21385, 738), LEECH_ENERGY(
                        21387, 739), LEECH_SPECIAL_ATTACK(21389, 940), WRATH(21391, 741), SOUL_SPLIT(
                        22539, 628), TURMOIL(22541, 629);
 
        private int action3;
        private int settingId;
 
        ePrayer(int action3, int settingId) {
                this.settingId = settingId;
                this.action3 = action3;
        }
 
        public int getAction3() {
                return action3;
        }
 
        public int getSettingId() {
                return settingId;
        }
 
    
     

        public boolean isActive() {
    		return Settings.getSetting(settingId) == 1;
    	}
        public void togglePrayer() {
                
                    
                        Menu.sendAction(169, -1, -1, action3);
                        Time.sleep(75);
                
        }
 
}