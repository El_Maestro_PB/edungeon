package org.ethan.dungeoneering.edungeon.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.ethan.dungeoneering.edungeon.api.Calculations;
import org.ethan.dungeoneering.edungeon.strategies.Glitched;
import org.ethan.dungeoneering.edungeon.strategies.HandleLevel;
import org.ethan.dungeoneering.edungeon.strategies.HandleNpcs;
import org.ethan.dungeoneering.edungeon.strategies.HandlePrayer;
import org.ethan.dungeoneering.edungeon.strategies.EnterDungeon;
import org.ethan.dungeoneering.edungeon.strategies.FormParty;
import org.ethan.dungeoneering.edungeon.strategies.Login;
import org.ethan.dungeoneering.edungeon.strategies.FloorPrep;
import org.ethan.dungeoneering.edungeon.ui.GUI;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.api.utils.Timer;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;
import org.rev317.min.api.wrappers.Tile;

/**
 * @author Ethan
 * @author Empathy
 */

@ScriptManifest(name = "eDungeon",
        category = Category.DUNGEONEERING,
        description = "Ikov Team Dungeoneering Script",
        author = "Ethan",
        servers = {"Ikov"},
        version = 1.0)

public class eDungeon extends Script implements Paintable, MessageListener {
	private final ArrayList<Strategy> strategies = new ArrayList<Strategy>();
	Timer run = new Timer();
    GUI g = new GUI();
   public static boolean guiWait = false;
   
    public boolean onExecute() {
    	g.setVisible(true);
        while (guiWait == false) {
            Time.sleep(500);
        }

        run.reset();
    	strategies.add(new HandlePrayer());
		strategies.add(new Login());
		strategies.add(new HandleLevel());
		strategies.add(new HandleNpcs());
		strategies.add(new EnterDungeon());
		strategies.add(new FormParty());
		strategies.add(new Glitched());
		strategies.add(new FloorPrep());
		provide(strategies);
        return true;
    }
    
    public void drawTile(final Graphics g, final Tile tile, final Color c) {
        final Point pn = Calculations.tileToScreen(tile, 0, 0, 0);
        final Point px = Calculations.tileToScreen(new Tile(tile.getX() + 1, tile.getY()), 0, 0, 0);
        final Point py = Calculations.tileToScreen(new Tile(tile.getX(), tile.getY() + 1), 0, 0, 0);
        final Point pxy = Calculations.tileToScreen(new Tile(tile.getX() + 1, tile.getY() + 1), 0, 0, 0);
        if (pn.x > 518 || pn.y > 340 || pxy.x > 518 || pxy.y > 340) {
                return;
        }
        g.setColor(c);
        g.drawPolygon(new int[] { py.x, pxy.x, px.x, pn.x }, new int[] { py.y, pxy.y, px.y, pn.y }, 4);
        g.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 100));
        g.fillPolygon(new int[] { py.x, pxy.x, px.x, pn.x }, new int[] { py.y, pxy.y, px.y, pn.y }, 4);
    }
   
	
    private Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            return null;
        }
    }

    private final Color color1 = new Color(0, 0, 0);

    private final Font font1 = new Font("Andalus", 1, 25);
    private final Font font2 = new Font("Arial", 1, 12);
    private final Font font3 = new Font("OCR A Extended", 1, 12);
    private final Image img1 = getImage("http://i.gyazo.com/41e11077d4bab5735ef301f5a9e31e8e.png");
    private final Image img2 = getImage("http://www.tip.it/runescape/images/items/9350.gif");

    public void paint(Graphics g1) {
        Graphics2D g = (Graphics2D)g1;
		 final org.rev317.min.api.wrappers.Npc[] n1 = Utils.getNPC();
		    org.rev317.min.api.wrappers.Npc n = null;
		    if (n1.length > 0)
		     n = n1[0];
		    if(n != null) { 	
		    	int health = n.getHealth();
		    	if(health == 0 || health < 1) {
		    		health = 100;
		    	}
	    		Point p = Calculations.getCenterPointOnScreen(n.getLocation().getRegionX(), n.getLocation().getRegionY());
	            g.setFont(font3);
	    		g.setColor(Color.RED);
	    		g.drawString("HP: "+health, p.x-15 , p.y-50);
	    		Time.sleep(10);
	    		Color tileColor = Color.CYAN;
     		drawTile(g, n.getLocation(), tileColor);
     		Time.sleep(10);
		    }
        g.drawImage(img1, 0, 339, null);
        g.drawImage(img2, 9, 343, null);
        g.drawImage(img2, 8, 422, null);
        g.drawImage(img2, 460, 344, null);
        g.drawImage(img2, 460, 422, null);
        g.setFont(font1);
        g.setColor(color1);
        g.drawString("eDungeon", 204, 375);
        g.setFont(font2);
        g.drawString("Floors Completed / (hr) : "+Variables.floorCompleted+" - ("+run.getPerHour(Variables.floorCompleted) +")", 167, 448);
        g.drawString("Experience Gained / (hr) :", 167, 422);
        g.drawString("Status: "+Variables.status, 13, 472);
        g.drawString("RunTime: "+run.toString(), 207, 396);
    }
    @Override
    public void onFinish() {
    	System.out.println("Floors completed: " + Variables.floorCompleted);
    }
    //END: Code generated using Enfilade's Easel
    @Override
    public void messageReceived(MessageEvent m) {
        if (m.getMessage().equalsIgnoreCase("Oh dear, you died!")) {
            Variables.deadCount++;
            Variables.dungStart = false;
    		Variables.chest = false;
    		Variables.rockFound = false;
    		Variables.isPraying = false;
    		HandleNpcs.waited = false;
    		FormParty.formedParty = false;
        	FormParty.joinedParty = false;
        	FormParty.iClicked = false;
        	FormParty.iTyped = false;
        	Variables.doBoss = false;
            
        }

        if (m.getMessage().contains("Your boss")) {
        	System.out.println("We've found the boss");
        	Variables.doBoss = true;
        	Variables.rockFound = true;
        } 
        
        if (m.getMessage().contains("has joined your party")) {
			Variables.startDungeon = true;
		}
        
        if (m.getMessage().toLowerCase().contains("completed the team")) {
            Variables.floorCompleted++;
    		Variables.dungStart = false;
    		Variables.chest = false;
    		Variables.rockFound = false;
    		Variables.isPraying = false;
    		HandleNpcs.waited = false;
    		FormParty.formedParty = false;
        	FormParty.joinedParty = false;
        	FormParty.iClicked = false;
        	FormParty.iTyped = false;
        	Variables.doBoss = false;
        }
        
        if (m.getMessage().contains("has found the Daconia rock!")) {
            Variables.rockFound = true;
        }
        
        if (m.getMessage().contains("You force left the dungeon, therefore you didn't receive any exp.")) {
        	Variables.floorsGlitched++;
            Variables.floorCompleted++;
        		Variables.dungStart = false;
        		Variables.chest = false;
        		Variables.rockFound = false;
        		Variables.isPraying = false;
        		HandleNpcs.waited = false;
        		FormParty.formedParty = false;
            	FormParty.joinedParty = false;
            	FormParty.iClicked = false;
            	FormParty.iTyped = false;
            	Variables.doBoss = false;
        }
        if (m.getMessage().contains("We couldn't find a party with that")) {
        	FormParty.formedParty = false;
        	FormParty.joinedParty = false;
        	FormParty.iClicked = false;
        	FormParty.iTyped = false;
        }

    }
}
