package org.ethan.dungeoneering.edungeon.strategies;

import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.wrappers.Npc;

/**
 * @author Empathy
 * @author Ethan
 */

public class EnterDungeon implements Strategy {
	
	/**
	 * The ID for the NPC Thok.
	 */
    final private int DUNGMASTER_ID = 9713;
    
    /**
     * The banker ID.
     */
    private final int BANKER = 494;
    
    /** 
     * An array made to hold the ID of THOK.
     */
    private Npc[] dungMaster;

    @Override
    public boolean activate() {
    	return Variables.start && Constants.START_ZONE.inTheZone() && Game.getOpenBackDialogId() != 4900;
    }

    @Override
    public void execute() {
    	
    	/**
    	 * If the Inventory isn't empty, everything will be banked.
    	 */
    	if (!Inventory.isEmpty()) {
    		
    		Variables.status = " Banking items....";
    		
			Npc[] banker = Npcs.getNearest(BANKER);
			
			if (banker.length > 0 && banker[0].distanceTo() < 15) {
				
				banker[0].interact(0);
				
				Time.sleep(new SleepCondition() {
		                @Override
		                public boolean isValid() {
		                    return Utils.bankIsOpen();
		                }
		            }, 2500);
				
				Utils.depositAllExcept();
				
				Time.sleep(new SleepCondition() {
	                @Override
	                public boolean isValid() {
	                    return Inventory.isEmpty();
	                }
	            }, 2500);
				
				Bank.close();
				
				Time.sleep(new SleepCondition() {
	                @Override
	                public boolean isValid() {
	                    return !Utils.bankIsOpen();
	                }
	            }, 2500);
			}
		}
    	
    	/**
    	 * If the Inventory is empty, the script will enter the dungeon.
    	 */
		if (Inventory.isEmpty() && !Utils.bankIsOpen()) {
			try {
				dungMaster = Npcs.getNearest(DUNGMASTER_ID);

				Variables.status = " Going into lobby...";

				if (dungMaster[0] != null && dungMaster.length > 0 && Game.getOpenBackDialogId() == -1) {
					while (Game.getOpenBackDialogId() != 4887) {
						dungMaster[0].interact(0);
						Time.sleep(1000);
						Time.sleep(new SleepCondition() {
							@Override
							public boolean isValid() {
								return Game.getOpenBackDialogId() == 4887;
							}
						}, 5000);
					}
				}
				while (Game.getOpenBackDialogId() == 4887) {
					Menu.sendAction(679, -1, -1, 4892, 48454, 1);
					Time.sleep(250);
				}

				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 2469;
					}
				}, 500);

				while (Game.getOpenBackDialogId() == 2469) {
					Menu.sendAction(315, -1, -1, 2471, -1, 1);
					Time.sleep(250);
				}

				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 2459;
					}
				}, 500);

				while (Game.getOpenBackDialogId() == 2459) {
						// enter team dung
						Menu.sendAction(315, -1, -1, 2462, -1, 1);
						Time.sleep(3000);
					}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
