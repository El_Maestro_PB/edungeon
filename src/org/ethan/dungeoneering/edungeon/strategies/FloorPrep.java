package org.ethan.dungeoneering.edungeon.strategies;


import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.SceneObject;


/**
 * @author Ethan
 */

public class FloorPrep implements Strategy {



	
   
	boolean waited = false;
  

    @Override
    public boolean activate() {

		    if(Variables.rockFound == false 
		    		&& Variables.doBoss == false
		    		&& !Constants.LOBBY.inTheZone()
		    		&& !Constants.START_ZONE.inTheZone()
		    		&& Skill.HITPOINTS.getLevel() > 50 
					&& Skill.PRAYER.getLevel() > 35
					&& !Utils.needPrayer()
					&& !Utils.isGlitched()) {
		    	
        return true;
		    }
		    
        return false;
    }

    @Override
    public void execute() {
   	 if(!Utils.needStopped()) {
 	 		final SceneObject[] root1 = SceneObjects.getNearest(Constants.rootId2);
    	 SceneObject root = null;
		    if (root1.length > 0)
		     root = root1[0];
		    
		    
    	if(!Variables.chest) {
    		if(waited == false) {
        		Time.sleep(1000);
        		waited = true;
        	}			
		
		if(Inventory.getCount(Constants.armours) > 0) {
			System.out.println("Equipping armour...");
			Utils.equipArmour();
		}
	

		if (!Variables.chest && Inventory.getCount(Constants.armours) < 1 ) {
			waited = false;
			try {
				SceneObject[] resource = SceneObjects.getNearest(Constants.chestId);
				if (resource != null && resource.length > 0) {
					Variables.status = "Gathering supplies from chest...";
					resource[0].interact(0);
					Time.sleep(new SleepCondition() {
						@Override
						public boolean isValid() {
							return Inventory.containts(Constants.overloadId);
						}
					}, 5000);
					if(Inventory.getCount(Constants.overloadId) > 0) {
					Variables.chest = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    	
		
		while (Inventory.containts(Constants.overloadId)) {
			Variables.status = "Drinking overload...";
			Utils.drink(Constants.overloadId);
		}
    	}
		if(Variables.chest) {
			
			if(Constants.bossTile.distanceTo() >= 4 && Variables.joiner == true) {
				Variables.status = "Sleeping while walking...";
				Constants.bossPath.traverse();
				Time.sleep(1000);
			}
			if (Constants.bossTile.distanceTo() < 4 && Variables.joiner == true && Variables.doBoss == false) {
				
				try {
					if(Variables.joiner == true) {

						Variables.status = "Attempting to enter boss's dungeon...";
						if(!Players.getMyPlayer().isInCombat()) {
						if (root != null) {
							if(Skill.HITPOINTS.getLevel() > 50 && Skill.PRAYER.getLevel() > 35) {
							Utils.handlePots();
							root.interact(0);
							Time.sleep(1000);
							Utils.handlePots();
							} else {
								Utils.handlePots();
							}
	
						}
					} else {
						Variables.status = "Sleeping while under attack...";
						Time.sleep(1000);
						
					} 
						
					
				
					
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
		if(Constants.rootTile.distanceTo() >= 4 && Variables.former == true){
  
					Variables.status = "Sleeping while walking...";
					Constants.rootPath.traverse();
					Time.sleep(1000);
				}
		
		

		if (Constants.rootTile.distanceTo() < 4 && !Variables.rockFound && Variables.former == true) {
		
			try {
				if(Variables.former == true) {
				while (Inventory.getCount(794) < 1) {
					SceneObject[] roots = SceneObjects.getNearest(Constants.rootId);
					Variables.status = "Attempting to find the rock....";
					Utils.gettingDeathAnim();
			

					if (roots != null && roots.length > 0) {
						roots[0].interact(0);
						Time.sleep(new SleepCondition() {
							@Override
							public boolean isValid() {
								return Inventory.getCount(794) > 0;
							}
						}, 500);
					}
				}
			
				
				}
				if(Inventory.getCount(794) > 0) {
					Variables.rockFound = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		}
		} else {
			System.out.println("eDungeon is stopping, either you don't have access or your trial is up.");
			Utils.setScriptState(Script.STATE_STOPPED);
		}
	}
}

