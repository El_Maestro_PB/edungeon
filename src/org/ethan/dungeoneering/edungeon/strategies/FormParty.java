package org.ethan.dungeoneering.edungeon.strategies;

import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;

/**
 * A class that joins and forms parties.
 * @author Masood
 * @author Ethan
 *
 */
public class FormParty implements Strategy  {

	
	public static boolean formedParty;
	public static boolean joinedParty;
	public static boolean iTyped = false;
	public static boolean iClicked = false;
	public boolean activate() {
		return Constants.LOBBY.inTheZone();
	}
	
	public void execute() {
		 if(!Utils.needStopped()) {
	 	  	if(Utils.needDeactivate()) {
			Utils.deactivatePray();
			Time.sleep(500);
		}
		if (Variables.formParty && !formedParty && !Variables.startDungeon) {
			Variables.status = "Forming a party...";
			Mouse.getInstance().click(609, 184, true);
			Time.sleep(500);
			if(Game.getOpenBackDialogId() != 2459) {
				Menu.sendAction(646, -1, -1, 27129, -1, 1);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 2459;
					}
				}, 3000);
			}
			
			if(Game.getOpenBackDialogId() == 2459) {
				iClicked = true;
				Menu.sendAction(315, -1, -1, 2461, -1, 1);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() != 2459;
					}
				}, 3000);		
			}
			
			if(iClicked == true) {
			formedParty = true;
			}
		}
		
		//Joins a party
		if (!Variables.startDungeon && !joinedParty && !Variables.formParty) {
			Variables.status = "Joining party...";
			Mouse.getInstance().click(609, 184, true);
			Time.sleep(500);
			if(Game.getOpenBackDialogId() != 2459) {
				Menu.sendAction(646, -1, -1, 27129, -1, 1);
				Time.sleep(250);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 2459;
					}
				}, 3000);
			}
			
			if(Game.getOpenBackDialogId() == 2459) {
				Menu.sendAction(315, -1, -1, 2462, -1, 1);
				Time.sleep(2000);	
				Keyboard.getInstance().sendKeys(Variables.joinParty);
				iTyped = true;
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() != 2459;
					}
				}, 3000);
			}
			if(iTyped == true) {
				joinedParty = true;
			}
	
		}
		if (Variables.startDungeon) {
			Variables.status = "Starting the dungeon...";
			Menu.sendAction(646, -1, -1, 26232, -1, 1);
			Time.sleep(6000);
			Variables.startDungeon = false;
			joinedParty = false;
			iTyped = false;
			formedParty = false;
			iClicked = false;
			Variables.dungStart = true;
		}
		
	 	
		} else {
			System.out.println("eDungeon is stopping, either you don't have access or your trial is up.");
			Utils.setScriptState(Script.STATE_STOPPED);
		}
	}
	
		
}
