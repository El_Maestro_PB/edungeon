package org.ethan.dungeoneering.edungeon.strategies;

import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;



public class Glitched implements Strategy {
    
        @Override
        public boolean activate() {
        	
              if(Utils.isGlitched())
              {
            	
            	 return true;
              
        	}
    
       
                return false; 
        }
 

		@Override
        public void execute() {
			Variables.status = "Boss is glitched, force quitting.";
			Utils.leaveParty();
			Time.sleep(750);
			
	
        }
}