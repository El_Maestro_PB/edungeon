package org.ethan.dungeoneering.edungeon.strategies;

import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;



public class HandleLevel implements Strategy {
    
        @Override
        public boolean activate() {
        	
              if(Utils.getHpPercent() <= 50  
            		  && Inventory.getItems(Constants.FOOD1) != null
            		  && !Constants.LOBBY.inTheZone()
            		  && !Constants.START_ZONE.inTheZone()
            		  && !Utils.isGlitched()
            		  )
              {
            	
            	 return true;
              
        	}
              if(Skill.PRAYER.getLevel() <= 35 
            		  && Inventory.getItems(Constants.RESTORES) != null
            		  && !Constants.LOBBY.inTheZone()
            		  && !Constants.START_ZONE.inTheZone()
            		  && !Utils.isGlitched()) {
            	  return true;
              }
       
                return false; 
        }
 

		@Override
        public void execute() {

	        	if(Utils.getHpPercent() <= 50 ) {
	    			Variables.status = "Attempting to eat...";
	        		if(Inventory.getCount(Constants.FOOD1) > 0 && Inventory.getItems(Constants.FOOD1) != null)  {
	        			for(Item i : Inventory.getItems(Constants.FOOD1)) {
	        				if(i != null) {
	        					if(Utils.getHpPercent() <= 50 ) {
	        						Time.sleep(200);
	        						Menu.sendAction(74, i.getId()-1, i.getSlot(), 3214);
	        						Time.sleep(1550);
	        						Skill.HITPOINTS.getLevel();
	        					}
	        				}
	        			}
	        		}
	        	}
	        	if(Skill.PRAYER.getLevel() <= 35) {
	    			Variables.status = "Attempting to restore prayer...";
	        		if(Inventory.getCount(Constants.RESTORES) > 0 && Inventory.getItems(Constants.RESTORES) != null)  {
	        			for(Item i : Inventory.getItems(Constants.RESTORES)) {
	        				if(i != null) {
	        					if(Skill.PRAYER.getLevel() <= 35) {
	        						Time.sleep(200);
	        						Menu.sendAction(74, i.getId()-1, i.getSlot(), 3214);
	        						Time.sleep(1550);
	        						Skill.PRAYER.getLevel();
	        					}
	        				}
	        			}
	        		}
	        	}
        }
}