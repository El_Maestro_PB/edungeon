package org.ethan.dungeoneering.edungeon.strategies;

import java.util.ArrayList;

import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.Tile;

/**
 * This class handles npcs in the dungeon.
 * 
 * @author Ethan
 * 
 */
public class HandleNpcs implements Strategy {

	int tries = 0;
	public static Npc npcs;
	public static boolean waited = false;
	public static ArrayList<Tile> badNPC = new ArrayList<Tile>();
	Tile center = new Tile(2468, 9894);

	public final static Filter<Npc> FILTER = new Filter<Npc>() {
		@Override
		public boolean accept(Npc n) {

			if (!badNPC.contains(n.getLocation()))
				if (!Constants.BAD_AREA.contains(n.getLocation()))
					if (!Constants.BAD_AREA2.contains(n.getLocation()))
						return true;
			return false;
		}

	};

	public boolean activate() {

		if (Variables.rockFound == true && Utils.getHpPercent() > 50
				&& Skill.PRAYER.getLevel() > 35 && !Constants.LOBBY.inTheZone()
				&& !Utils.needPrayer() && !Utils.isGlitched()) {
			if (Constants.BAD_AREA2.contains(Players.getMyPlayer()
					.getLocation())) {
				Constants.stuckPath.traverse();
				Time.sleep(1000);
				while (Utils.isWalking() && !Players.getMyPlayer().isInCombat()
						&& Utils.getHpPercent() > 50
						&& Skill.PRAYER.getLevel() > 35 && !Utils.isGlitched()) {
					Time.sleep(500);
				}

			}

			return true;

		}

		return false;
	}

	// [2434, 9882]
	public void execute() {
		if (Variables.joiner) {
			if (waited == false) {
				Time.sleep(6000);
				waited = true;
			}
		}
		if (!Utils.isGlitched()) {
			if (Variables.rockFound) {
				Utils.gettingDeathAnim();
				final Npc[] npcs1 = Npcs.getNearest(FILTER);
				if (npcs1.length > 0)
					npcs = npcs1[0];

				if (npcs != null) {
					Variables.status = "Handling dungeon NPCs...";

					if (npcs != null && npcs.distanceTo() <= 5
							&& !Utils.isGlitched()) {

						if (!Players.getMyPlayer().isInCombat()) {
							Variables.status = "Attempting to attack NPC...";
							npcs.interact(1);
							Time.sleep(1000);
							tries++;
							if (Utils.attackingBack()) {
								tries = 0;
							}
							while (Utils.isWalking()
									&& Utils.getHpPercent() > 50
									&& Skill.PRAYER.getLevel() > 35
									&& !Utils.isGlitched()) {
								Variables.status = "Sleeping while walking....";
								Time.sleep(500);

							}

							if (tries > 6) {
								Variables.status = "Glitched NPC, fixing the problem...";
								badNPC.add(npcs.getLocation());
								npcs = null;
								tries = 0;
							}
							Time.sleep(new SleepCondition() {
								@Override
								public boolean isValid() {
									return Players.getMyPlayer().isInCombat();
								}
							}, 1000);
						}
						while (Players.getMyPlayer().isInCombat()
								&& Utils.getHpPercent() > 50
								&& Skill.PRAYER.getLevel() > 35
								&& !Utils.isGlitched() || Utils.attackingBack()
								&& Utils.getHpPercent() > 50
								&& Skill.PRAYER.getLevel() > 35
								&& !Utils.isGlitched()) {
							Variables.status = "Sleeping while under attack...";
							Time.sleep(250);
							tries = 0;
						}
					} else if (npcs != null && npcs.distanceTo() > 5) {
						Variables.status = "Walking to NPC...";
						npcs.getLocation().walkTo();

						Time.sleep(1000);
						
					}

				} else if (npcs == null) {
					npcs = null;
					Variables.status = "No NPCs nearby, walking to center....";
					center.walkTo();
					Time.sleep(1000);

				}
			}

		}
	}
}
