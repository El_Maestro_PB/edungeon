package org.ethan.dungeoneering.edungeon.strategies;

import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;



public class HandlePrayer implements Strategy {
    
        @Override
        public boolean activate() {
        	
        	if(Utils.needPrayer()
        			&& !Utils.isGlitched()) {
        		return true;
        	}
       
                return false; 
        }
 

		@Override
        public void execute() {
			Variables.status = " Handling prayer....";
			Utils.activatePray();
			Time.sleep(1000);
	        
        }
}