package org.ethan.dungeoneering.edungeon.strategies;

import java.awt.event.KeyEvent;

import org.ethan.dungeoneering.edungeon.util.Utils;
import org.ethan.dungeoneering.edungeon.util.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;

/**
 * A class that relogs on Ikov.
 * @author Masood
 * @author Ethan
 * 
 */
public class Login implements Strategy {
	@Override
	public boolean activate() {
		return !Utils.isLoggedIn();
	}

	@Override
	public void execute() {
		if (!Utils.isLoggedIn()) {
			Variables.dungStart = false;
    		Variables.chest = false;
    		Variables.rockFound = false;
    		Variables.isPraying = false;
    		HandleNpcs.waited = false;
    		FormParty.formedParty = false;
        	FormParty.joinedParty = false;
        	FormParty.iClicked = false;
        	FormParty.iTyped = false;
        	Variables.doBoss = false;
			Variables.status = "Logging in...";
			Time.sleep(5000);
			Keyboard.getInstance().clickKey(KeyEvent.VK_ENTER);
			Time.sleep(1500); 
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return Utils.isLoggedIn();
				}
			}, 5000);
			if (Utils.isLoggedIn()) {
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 4900;
					}
				}, 2500);
				Menu.sendAction(679, -1, -1, 4907, -1, 1);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() != 4900;
					}
				}, 2500);
			}
		}
	}
}