package org.ethan.dungeoneering.edungeon.ui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import org.ethan.dungeoneering.edungeon.main.eDungeon;
import org.ethan.dungeoneering.edungeon.util.Constants;
import org.ethan.dungeoneering.edungeon.util.Variables;

public class GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4531651966521060407L;
	public GUI() {
		initComponents();
	}

	private void startActionPerformed(ActionEvent e) {
		int armour[] = { Integer.parseInt(helmSlot.getText()), Integer.parseInt(plateSlot.getText()), Integer.parseInt(legSlot.getText()), Integer.parseInt(bootSlot.getText()), Integer.parseInt(wepSlot.getText()), Integer.parseInt(shieldSlot.getText()) };
		Constants.armours = armour;
		Variables.start = true;
		if (join.isSelected()) {
			Variables.joinParty = username.getText();
		
		}
		if (form.isSelected()) {
			Variables.formParty = true;
		
		}
		if(attack.isSelected()) {
			Variables.former = true;
		}
		if(leech.isSelected()) {
			Variables.joiner = true;
		}
		if (ancient.isSelected()) {
			Variables.curses = true;
		} 
		this.dispose();
		eDungeon.guiWait = true;
	}

	private void saveActionPerformed(ActionEvent e) {
		try {
			  JFileChooser chooser = new JFileChooser();
			  File directory = new File(chooser.getCurrentDirectory()
	 					+ "/Parabot");
			  chooser.setCurrentDirectory(directory);
			  chooser.setFileFilter(new FileFilter() {
			        @Override
			        public boolean accept(File f) {
			            if (f.isDirectory()) {
			                return true;
			            }
			            final String name = f.getName();
			            return name.endsWith(".dat");
			        }

			        @Override
			        public String getDescription() {
			            return "*.dat";
			        }
			    });
			   int retrival = chooser.showSaveDialog(null);
			    if (retrival == JFileChooser.APPROVE_OPTION) {
			File file = new File(directory+"/"+chooser.getSelectedFile().getName()+".dat");
			if (file.exists()) {
				file.delete();
			} else {
				file.createNewFile();
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(chooser.getSelectedFile()+".dat"));
			writer.write("" + Integer.parseInt(helmSlot.getText()));
			writer.newLine();
			writer.write("" + Integer.parseInt(plateSlot.getText()));
			writer.newLine();
			writer.write("" + Integer.parseInt(legSlot.getText()));
			writer.newLine();
			writer.write("" + Integer.parseInt(bootSlot.getText()));
			writer.newLine();
			writer.write("" + Integer.parseInt(wepSlot.getText()));
			writer.newLine();
			writer.write("" + Integer.parseInt(shieldSlot.getText()));
			writer.flush();
			writer.close();
			System.out.println("Data Saved");
			    }
		} catch (UnsupportedEncodingException e1) {
			throw new RuntimeException(e1);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	

	}

	private void loadActionPerformed(ActionEvent e) {
		try {
			JFileChooser chooser = new JFileChooser();
			  File directory = new File(chooser.getCurrentDirectory()
	 					+ "/Parabot");
			  chooser.setCurrentDirectory(directory);
			  chooser.setFileFilter(new FileFilter() {
			        @Override
			        public boolean accept(File f) {
			            if (f.isDirectory()) {
			                return true;
			            }
			            final String name = f.getName();
			            return name.endsWith(".dat");
			        }

			        @Override
			        public String getDescription() {
			            return "*.dat";
			        }
			    });
			   int retrival = chooser.showOpenDialog(null);
			    if (retrival == JFileChooser.APPROVE_OPTION) {
			File dataContiner = new File(directory+"/"+chooser.getSelectedFile().getName());
			BufferedReader bufferedReader = new BufferedReader(new FileReader(dataContiner));
			helmSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			plateSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			legSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			bootSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			wepSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			shieldSlot.setText("" + Integer.parseInt(bufferedReader.readLine()));
			bufferedReader.close();
			System.out.println("Data loaded");
			    }
		} catch (FileNotFoundException e1) {
			throw new RuntimeException("Cannot find data.");
		} catch (IOException e2) {
			throw new RuntimeException(e2);
		}


	}
	
	private void formStateChanged(ChangeEvent e) {
		if(form.isSelected()) {
			username.setEnabled(false);
		}else{
			username.setEnabled(true);
		}
	}
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - kalissa phelps
		tabbedPane1 = new JTabbedPane();
		panel1 = new JPanel();
		//label2 = new JLabel();
		form = new JRadioButton();
		join = new JRadioButton();
		username = new JTextField();
		label3 = new JLabel();
		ancient = new JRadioButton();
		normal = new JRadioButton();
		attack = new JRadioButton();
		leech = new JRadioButton();
		panel3 = new JPanel();
		helmSlot = new JTextField();
		textField2 = new JTextField();
		plateSlot = new JTextField();
		textField6 = new JTextField();
		wepSlot = new JTextField();
		textField8 = new JTextField();
		textField9 = new JTextField();
		shieldSlot = new JTextField();
		textField11 = new JTextField();
		legSlot = new JTextField();
		textField13 = new JTextField();
		bootSlot = new JTextField();
		button4 = new JButton();
		button5 = new JButton();
		label4 = new JLabel();
		label5 = new JLabel();
		panel2 = new JPanel();
		label1 = new JLabel();
		button3 = new JButton();

		//======== this ========
		setTitle("eDungeon - Ikov Dungeoneering");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		//======== tabbedPane1 ========
		{

			//======== panel1 ========
			{

				// JFormDesigner evaluation mark
				panel1.setBorder(new javax.swing.border.CompoundBorder(
					new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
						"", javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
						java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

				panel1.setLayout(null);
				String path = "http://i.imgur.com/NbCOLAR.jpg";
				URL url;

				try {
					url = new URL(path);
					BufferedImage image = ImageIO.read(url);
					JLabel labels = new JLabel(new ImageIcon(image));
					labels.setBounds(25, 10, 395, 80);
					panel1.add(labels);
				} catch (Exception e) {
					e.printStackTrace();
				}

				//---- label2 ----
//				label2.setText("Reserved for logo");
//				label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
//				label2.setHorizontalAlignment(SwingConstants.CENTER);
//				panel1.add(label2);
//				label2.setBounds(25, 10, 395, 80);

				//---- form ----
				form.setText("Form Party");
				form.addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						formStateChanged(e);
					}
				});
				panel1.add(form);
				form.setBounds(new Rectangle(new Point(180, 140), form.getPreferredSize()));

				//---- join ----
				join.setText("Join Party");
				panel1.add(join);
				join.setBounds(180, 100, 79, 23);
				join.setSelected(true);

				//---- username ----

				panel1.add(username);
				username.setBounds(150, 220, 150, username.getPreferredSize().height);
				username.setEnabled(false);
				//---- label3 ----
				label3.setText("Leader's Username");
				label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
				panel1.add(label3);
				label3.setBounds(new Rectangle(new Point(170, 195), label3.getPreferredSize()));

				//---- ancient ----
				ancient.setText("Ancient Prayer");
				panel1.add(ancient);
				ancient.setBounds(new Rectangle(new Point(65, 255), ancient.getPreferredSize()));

				//---- normal ----
				normal.setText("Normal Prayer");
				panel1.add(normal);
				normal.setBounds(new Rectangle(new Point(265, 255), normal.getPreferredSize()));

				//---- attack ----
				attack.setText("Attack");
				panel1.add(attack);
				attack.setBounds(new Rectangle(new Point(85, 280), attack.getPreferredSize()));

				//---- leech ----
				leech.setText("Leech");
				panel1.add(leech);
				leech.setBounds(new Rectangle(new Point(290, 280), leech.getPreferredSize()));

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel1.getComponentCount(); i++) {
						Rectangle bounds = panel1.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel1.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel1.setMinimumSize(preferredSize);
					panel1.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Team", panel1);

			//======== panel3 ========
			{
				panel3.setLayout(null);

				//---- helmSlot ----
				helmSlot.setHorizontalAlignment(SwingConstants.CENTER);
				helmSlot.setText("0");
				panel3.add(helmSlot);
				helmSlot.setBounds(175, 45, 95, helmSlot.getPreferredSize().height);

				//---- textField2 ----
				textField2.setText("Helmet ID");
				textField2.setEditable(false);
				textField2.setFont(textField2.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField2.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField2);
				textField2.setBounds(180, 10, 80, 30);

				//---- plateSlot ----
				plateSlot.setHorizontalAlignment(SwingConstants.CENTER);
				plateSlot.setText("0");
				panel3.add(plateSlot);
				plateSlot.setBounds(175, 125, 100, 20);

				//---- textField6 ----
				textField6.setText("PlateBody ID");
				textField6.setEditable(false);
				textField6.setFont(textField6.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField6.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField6);
				textField6.setBounds(180, 90, 85, 30);

				//---- wepSlot ----
				wepSlot.setHorizontalAlignment(SwingConstants.CENTER);
				wepSlot.setText("0");
				panel3.add(wepSlot);
				wepSlot.setBounds(50, 125, 100, 20);

				//---- textField8 ----
				textField8.setText("Weapon ID");
				textField8.setEditable(false);
				textField8.setFont(textField8.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField8.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField8);
				textField8.setBounds(55, 90, 85, 30);

				//---- textField9 ----
				textField9.setText("Shield ID");
				textField9.setEditable(false);
				textField9.setFont(textField9.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField9.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField9);
				textField9.setBounds(305, 90, 85, 30);

				//---- shieldSlot ----
				shieldSlot.setHorizontalAlignment(SwingConstants.CENTER);
				shieldSlot.setText("0");
				panel3.add(shieldSlot);
				shieldSlot.setBounds(300, 125, 100, 20);

				//---- textField11 ----
				textField11.setText("Legs ID");
				textField11.setEditable(false);
				textField11.setFont(textField11.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField11.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField11);
				textField11.setBounds(180, 170, 85, 30);

				//---- legSlot ----
				legSlot.setHorizontalAlignment(SwingConstants.CENTER);
				legSlot.setText("0");
				panel3.add(legSlot);
				legSlot.setBounds(175, 205, 100, 20);

				//---- textField13 ----
				textField13.setText("Boots ID");
				textField13.setEditable(false);
				textField13.setFont(textField13.getFont().deriveFont(Font.BOLD|Font.ITALIC));
				textField13.setHorizontalAlignment(SwingConstants.CENTER);
				panel3.add(textField13);
				textField13.setBounds(180, 240, 85, 30);

				//---- bootSlot ----
				bootSlot.setHorizontalAlignment(SwingConstants.CENTER);
				bootSlot.setText("0");
				panel3.add(bootSlot);
				bootSlot.setBounds(175, 275, 100, 20);

				//---- button4 ----
				button4.setText("Save Equipment");
				button4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						saveActionPerformed(e);
					}
				});
				panel3.add(button4);
				button4.setBounds(new Rectangle(new Point(25, 275), button4.getPreferredSize()));

				//---- button5 ----
				button5.setText("Load Equipment");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						loadActionPerformed(e);
					}
				});
				panel3.add(button5);
				button5.setBounds(320, 275, 109, 23);

				//---- label4 ----
				label4.setText("Leave \"0\" if empty");
				label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
				panel3.add(label4);
				label4.setBounds(new Rectangle(new Point(340, 5), label4.getPreferredSize()));

				//---- label5 ----
				label5.setText("Leave \"0\" if empty");
				label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
				panel3.add(label5);
				label5.setBounds(10, 5, 106, 14);

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel3.getComponentCount(); i++) {
						Rectangle bounds = panel3.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel3.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel3.setMinimumSize(preferredSize);
					panel3.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Equipment", panel3);

			//======== panel2 ========
			{
				panel2.setLayout(null);

				//---- label1 ----
				label1.setText("Under Development");
				label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 10f));
				panel2.add(label1);
				label1.setBounds(new Rectangle(new Point(120, 140), label1.getPreferredSize()));

				{ // compute preferred size
					Dimension preferredSize = new Dimension();
					for(int i = 0; i < panel2.getComponentCount(); i++) {
						Rectangle bounds = panel2.getComponent(i).getBounds();
						preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
						preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
					}
					Insets insets = panel2.getInsets();
					preferredSize.width += insets.right;
					preferredSize.height += insets.bottom;
					panel2.setMinimumSize(preferredSize);
					panel2.setPreferredSize(preferredSize);
				}
			}
			tabbedPane1.addTab("Solo", panel2);
		}
		contentPane.add(tabbedPane1);
		tabbedPane1.setBounds(0, 0, 460, 335);

		//---- button3 ----
		button3.setText("Start eDungeon");
		button3.setFont(button3.getFont().deriveFont(button3.getFont().getStyle() | Font.BOLD));
		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startActionPerformed(e);
			}
		});
		contentPane.add(button3);
		button3.setBounds(0, 335, 460, 36);

		{ // compute preferred size
			Dimension preferredSize = new Dimension();
			for(int i = 0; i < contentPane.getComponentCount(); i++) {
				Rectangle bounds = contentPane.getComponent(i).getBounds();
				preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
				preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
			}
			Insets insets = contentPane.getInsets();
			preferredSize.width += insets.right;
			preferredSize.height += insets.bottom;
			contentPane.setMinimumSize(preferredSize);
			contentPane.setPreferredSize(preferredSize);
		}
		pack();
		setLocationRelativeTo(getOwner());

		//---- buttonGroup1 ----
		ButtonGroup buttonGroup1 = new ButtonGroup();
		buttonGroup1.add(form);
		buttonGroup1.add(join);

		//---- buttonGroup2 ----
		ButtonGroup buttonGroup2 = new ButtonGroup();
		buttonGroup2.add(ancient);
		buttonGroup2.add(normal);

		//---- buttonGroup3 ----
		ButtonGroup buttonGroup3 = new ButtonGroup();
		buttonGroup3.add(attack);
		buttonGroup3.add(leech);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - kalissa phelps
	private JTabbedPane tabbedPane1;
	private JPanel panel1;
	//private JLabel label2;
	private JRadioButton form;
	private JRadioButton join;
	private JTextField username;
	private JLabel label3;
	private JRadioButton ancient;
	private JRadioButton normal;
	private JRadioButton attack;
	private JRadioButton leech;
	private JPanel panel3;
	private JTextField helmSlot;
	private JTextField textField2;
	private JTextField plateSlot;
	private JTextField textField6;
	private JTextField wepSlot;
	private JTextField textField8;
	private JTextField textField9;
	private JTextField shieldSlot;
	private JTextField textField11;
	private JTextField legSlot;
	private JTextField textField13;
	private JTextField bootSlot;
	private JButton button4;
	private JButton button5;
	private JLabel label4;
	private JLabel label5;
	private JPanel panel2;
	private JLabel label1;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
