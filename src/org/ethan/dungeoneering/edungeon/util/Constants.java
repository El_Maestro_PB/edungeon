package org.ethan.dungeoneering.edungeon.util;

import java.util.HashMap;
import org.ethan.dungeoneering.edungeon.api.eArea;
import org.rev317.min.api.wrappers.Area;
import org.rev317.min.api.wrappers.Tile;
import org.rev317.min.api.wrappers.TilePath;

public class Constants {
	
	
    public static int chestId = 5270;
    
    public static int overloadId = 15333;

    public static int rootId = 1986;
    
    public static int rootId2 = 2451;
    
    public static Tile[] bossTiles = { new Tile(2474, 9865), new Tile(2461, 9862), new Tile(2448, 9860), new Tile(2444, 9857), new Tile(2441, 9862), new Tile(2443, 9868), new Tile(2448, 9872),new Tile(2454, 9868), new Tile(2456, 9873), new Tile(2450, 9879), new Tile(2453, 9884), new Tile(2459, 9888), new Tile(2464, 9891), new Tile(2465, 9897), new Tile(2467, 9903) };

    public static Tile[] rootTiles = { new Tile(2474, 9865), new Tile(2461, 9862), new Tile(2448, 9860), new Tile(2444, 9857), new Tile(2441, 9862), new Tile(2443, 9868), new Tile(2448, 9872),new Tile(2454, 9868), new Tile(2456, 9873) };

    public static Tile[] stuckTiles = { new Tile(2443, 9882), new Tile(2449,9882), new Tile(2455, 9886), new Tile(2444, 9857), new Tile(2460, 9888), new Tile(2466, 9897), new Tile(2470, 9896) };
    
    public static TilePath rootPath = new TilePath(rootTiles);
    
    public static TilePath bossPath = new TilePath(bossTiles);
    
    public static TilePath stuckPath = new TilePath(stuckTiles);
    
    public static Tile rootTile = new Tile(2455, 9873);
    
    public static Tile bossTile = new Tile(2467, 9903);

    public static final int[] RESTORES = { 3025,  3027, 3029, 3031 };
    
	public static final eArea LOBBY = new eArea(new Tile(2842, 5158), new Tile(2851, 5142));
    
	public static final int[] FOOD1 = { 18166, 18156 }; 
    
    public static boolean trial = true, testing = false;
    
    public static final eArea START_ZONE =  new eArea(new Tile(3444, 3729), new Tile(3456, 3710));
    
    public static Area BAD_AREA = new Area(new Tile(2440,9866), new Tile(2440,9879), new Tile(2432,9879), new Tile(2432,9866));
    
    public static Area BAD_AREA2 = new Area(new Tile(2440,9879), new Tile(2440,9886), new Tile(2432,9879), new Tile(2432,9886));
  
    public static int armours[] = {};

    public static int boss = 10110;
   
    public static HashMap<String, Object> MAP = new HashMap<String, Object>();
	

}