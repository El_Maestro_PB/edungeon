package org.ethan.dungeoneering.edungeon.util;

import java.awt.Point;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import org.ethan.dungeoneering.edungeon.api.ePrayer;
import org.parabot.core.Context;
import org.parabot.core.asm.ASMClassLoader;
import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;
import org.rev317.min.api.wrappers.Npc;


public class Utils {
	 
	  public static final int DUNG_BOSSES[] = {10110, 10044, 9989, 10116, 10064, 9934};
	private static final Comparator<Npc> NEAREST_SORTER = new Comparator<Npc>() {

		@Override
		public int compare(Npc n1, Npc n2) {
			return n1.distanceTo() - n2.distanceTo();
		}

	};
	public static void depositAllExcept(int... itemIDs) {
		final ArrayList<Integer> dontDeposit = new ArrayList<Integer>();
		if (Inventory.getCount(false) <= 2) {
			return;
		} else {
			for (int i : itemIDs) {
				dontDeposit.add(i);
			}
		}
		for (Item inventoryItem : Inventory.getItems()) {
			if (!dontDeposit.contains(inventoryItem.getId())) {
				Menu.sendAction(431, inventoryItem.getId() - 1, inventoryItem.getSlot(), 5064, 2213, 3);
				Time.sleep(80);
			}
		}
	}
	public static boolean isGlitched() {
		if(Variables.doBoss == true) {
		 if (Npcs.getNearest(DUNG_BOSSES).length <= 0) {
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Npcs.getNearest(DUNG_BOSSES).length > 0;
                 }
            }, 5000);
            if (Npcs.getNearest(DUNG_BOSSES).length <= 0) {
            	System.out.println("Yeah bro, we glitched");
                return true;
            }
        }
		}
		return false;
	}
	public static boolean needStopped() {
		
		if(Constants.trial == true) {
		if(Variables.floorCompleted >= 2) {
			return true;
		}
		}
		
		if(Constants.testing == true) {
			if(!Utils.getName().equals("Ethan Is God") && !Utils.getName().equals("Empathy") && !Utils.getName().equals("Para Ethan") && !Utils.getName().equals("Lordgmage") && !Utils.getName().equals("Testingwalk")) {
			return true;
		}
		}
		
		return false;
	}
	

	public static ArrayList<Integer> hashs = new ArrayList<Integer>();
	public static final Npc[] getNPC() {
		final Npc[] npcs = Npcs.getNpcs(new Filter<Npc>() {
			

			@Override
			public boolean accept(Npc npc) {
				
					if (npc != null && Players.getMyPlayer().getInteractingCharacter() != null && Players.getMyPlayer().getInteractingCharacter().equals(npc)) {
						return true;
					}
				
				return false;
			}

		});
		Arrays.sort(npcs, NEAREST_SORTER);
		return npcs;
	}
	public static Object getInstance() {
		return (Object) Context.getInstance().getApplet();
	}

	 public static int[] getSetting() {
	    	try
			{
	    		
				   final Context context = Context.getInstance();
		            final ASMClassLoader classLoader = context.getASMClassLoader();
		            Time.sleep(25);
		            final Class<?> cls = classLoader.loadClass("b");
		            Time.sleep(25);
		            Object obj = getInstance();
		            Time.sleep(25);
				Field wlk = cls.getDeclaredField("fC");
				Time.sleep(25);
				wlk.setAccessible(true);
				Time.sleep(25);
				return (int[]) wlk.get(obj);
			
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
	    	return null;
	    }
	 public static void setScriptState(int state) {
			if (Context.getInstance().getRunningScript() != null) {
				Context.getInstance().getRunningScript().setState(state);
			}
		}
	 public static String getName() {
	    	try
			{
	    		
				   final Context context = Context.getInstance();
		            final ASMClassLoader classLoader = context.getASMClassLoader();
		            Time.sleep(25);
		            final Class<?> cls = classLoader.loadClass("ai");
		            Time.sleep(25);
		            Object obj = getPlayerObject();
		            Time.sleep(25);
				Field wlk = cls.getDeclaredField("aH");
				Time.sleep(25);
				wlk.setAccessible(true);
				Time.sleep(25);
				return (String) wlk.get(obj);
			
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
	    	return null;
	    }
    public static void gettingDeathAnim() {
		try {
			Npc[] npc = Npcs.getNpcs();
			for (Npc playa : npc) {
			
					if (playa != null) {
							if (playa.getAnimation() == 111) {
								if(!hashs.contains(playa.getDef().hashCode())) {
									Variables.npcDeaths++;
									System.out.println("NPC DIED: "+Variables.npcDeaths);
								}
							}
						}

			}
		} catch (Exception e) {
			System.out.println("shitty parabot api ;)");
		}
	
	}
    public static Object getPlayerObject() throws Exception{
		  String field = "hN";

     Class<?> clientClass = Loader.getClient().getClass();
     Field streamField1 = clientClass.getDeclaredField(field);
     Time.sleep(5);
     streamField1.setAccessible(true); // dunno if it's private or not, better to be safe then sorry
     Time.sleep(5);
     return streamField1.get(Loader.getClient()); // done.
}
	 public static int isMoving()  {
	   try{
		   String field = "N";
		   String clazz = "ag";
		   
		 Context context = Context.getInstance();
	   ASMClassLoader classLoader = context.getASMClassLoader();
	   Class<?> c = classLoader.loadClass(clazz);
	  Field wlk = c.getDeclaredField(field);
	   Time.sleep(10);
	   wlk.setAccessible(true);
	   Time.sleep(10);

		return (int) wlk.get(getPlayerObject());
		
	   } catch(Exception e){ 
		   e.printStackTrace();
	   }
		return -1;

	  }
	  public static boolean isWalking() {
		  return isMoving() != 0;
	  }
	
	/**
	 * A method that activates prayer.
	 */
	 public static boolean needPrayer() {
		 
		 if(!Constants.START_ZONE.inTheZone() && !Constants.LOBBY.inTheZone()) {
			if(Skill.PRAYER.getLevel() > 35) {
			 if (Variables.curses) {
				if(!ePrayer.TURMOIL.isActive() && Skill.getLevelByExperience(Skill.PRAYER.getExperience()) > 94 || !ePrayer.DEFLECT_MELEE.isActive()) {
					return true;
				}
			}else{
				if(!ePrayer.PROTECT_FROM_MELEE.isActive()){ 
					return true;
				}
			}
			}
		 }
		 
		 return false;
	 }
	 public static boolean needDeactivate() {
		 if(Constants.START_ZONE.inTheZone() || Constants.LOBBY.inTheZone()) {
				if (Variables.curses) {
					if(ePrayer.TURMOIL.isActive() || ePrayer.DEFLECT_MELEE.isActive()) {
						return true;
					}
				}else{
					if(ePrayer.PROTECT_FROM_MELEE.isActive()){ 
						return true;
					}
				}
			 }
			 
		 return false;
	 }
	 public static void deactivatePray() {
		 if (Variables.curses) {
				if(ePrayer.TURMOIL.isActive()) {
				Variables.status = "Deactivating prayer...";
				ePrayer.TURMOIL.togglePrayer();
				Time.sleep(1000);
				}
				if(ePrayer.DEFLECT_MELEE.isActive()) {
					Variables.status = "Deactivating prayer...";
					ePrayer.DEFLECT_MELEE.togglePrayer();
					Time.sleep(1000);
				}

				
			} else {
				if(ePrayer.PROTECT_FROM_MELEE.isActive()) {
					Variables.status = "Deactivating prayer...";
					ePrayer.PROTECT_FROM_MELEE.togglePrayer();
					Time.sleep(750);
				}
			}
		}
		
	public static void activatePray() {
		if (Variables.curses) {
			if(!ePrayer.TURMOIL.isActive() && Skill.getLevelByExperience(Skill.PRAYER.getExperience()) > 94) {
			Variables.status = "Activating prayer...";
			ePrayer.TURMOIL.togglePrayer();
			Time.sleep(1000);
			}
			if(!ePrayer.DEFLECT_MELEE.isActive()) {
				Variables.status = "Activating prayer...";
				ePrayer.DEFLECT_MELEE.togglePrayer();
				Time.sleep(1000);
			}

			
		} else {
			if(!ePrayer.PROTECT_FROM_MELEE.isActive()) {
				Variables.status = "Activating prayer...";
				ePrayer.PROTECT_FROM_MELEE.togglePrayer();
				Time.sleep(750);
			}
		}
	}
	
	/**
	 * Equips armour.
	 */
	public static void equipArmour() {
		Variables.status = "Equipping Armour...";
        Item[] armour = Inventory.getItems(Constants.armours);
        for (Item i : armour) {
            if (i != null) {
                Menu.sendAction(454, i.getId() - 1, i.getSlot(), 3214, 0, 5);
                Time.sleep(500);
            }
        }
	}
	/**
	 * attacking a npc
	 */
	public static int getNpcCount() {
		int x = 0;
		for(Npc npc : Npcs.getNpcs()) {
			if(npc != null) {
			x++;
			}
		}
		return x;
	}
    public static boolean attackingBack() {
		try {
			Npc[] npc = Npcs.getNpcs();
			for (Npc playa : npc) {
				if (playa != null) {
					if (Players.getMyPlayer().getInteractingCharacter() != null) {
						if (Players.getMyPlayer() != null) {
							if (Players.getMyPlayer().getInteractingCharacter().equals(playa)) {
								return true;
							}
						}
					}
				}

			}
		} catch (Exception e) {
			System.out.println("shitty parabot api ;)");
		}
		return false;
	}

	public static boolean bankIsOpen() {
		return Game.getOpenInterfaceId() == 5292;
	}
	

	
	public static void drink(int...potionId) {
		if (Inventory.getCount(potionId) > 0) {
			for (Item i : Inventory.getItems(potionId)) {
				if (i != null) {
					Menu.sendAction(74, i.getId() - 1, i.getSlot(), 3214);
					Time.sleep(200);
				}
			}
		}
	}

	public static int getHpPercent() {
		if (Skill.HITPOINTS.getRealLevel() > 99) {
			return Math.round((Skill.HITPOINTS.getLevel()*100)/ 99 );
		} else {
			return Math.round((Skill.HITPOINTS.getLevel()*100)/ Skill.HITPOINTS.getRealLevel());
		}
	}
	

	public static void dropAllExcept(int... itemIDs) {
		final ArrayList<Integer> dontDrop = new ArrayList<>();
		if (Inventory.getCount(false) <= 2) {
			return;
		} else {
			for (int i : itemIDs) {
				dontDrop.add(i);
			}
		}
		for (Item inventoryItem : Inventory.getItems()) {
			if (!dontDrop.contains(inventoryItem.getId())) {
				Menu.sendAction(847, inventoryItem.getId() - 1, inventoryItem.getSlot(), 3214);
				Time.sleep(80);
			}
		}
	}
	/**
	 * Formats a number as a string.
	 * @param number the number to format.
	 * @return the string formatted.
	 */
	public static String formatNumb(int number) {
		String numberString = String.valueOf(number);
		if (Integer.parseInt(numberString) < 1000) {
			return numberString;
		} else if (Integer.parseInt(numberString) > 1000 && Integer.parseInt(numberString) < 10000) {
			return numberString.charAt(0) + "." + numberString.charAt(1) + "k";
		} else if (Integer.parseInt(numberString) > 10000 && Integer.parseInt(numberString) < 100000) {
			return numberString.substring(0, 2) + "." + numberString.charAt(2) + "k";
		} else if (Integer.parseInt(numberString) > 100000 && Integer.parseInt(numberString) < 1000000) {
			return numberString.substring(0, 3) + "." + numberString.charAt(3) + "k";
		} else if (Integer.parseInt(numberString) > 1000000 && Integer.parseInt(numberString) < 10000000) {
			return numberString.charAt(0) + "." + numberString.charAt(1) + "m";
		} else if (Integer.parseInt(numberString) > 10000000 && Integer.parseInt(numberString) < 100000000) {
			return numberString.substring(0, 2) + "." + numberString.charAt(2) + "m";
		} else if (Integer.parseInt(numberString) > 100000000 && Integer.parseInt(numberString) < 1000000000) {
			return numberString.substring(0, 3) + "." + numberString.charAt(3) + "m";
		} else if (Long.valueOf(numberString) > 1000000000L && Long.valueOf(numberString) < 10000000000L) {
			return numberString.charAt(0) + "." + numberString.charAt(1) + "b";
		} else if (Long.valueOf(numberString) > 10000000000L && Long.valueOf(numberString) < 100000000000L) {
			return numberString.substring(0, 2) + "." + numberString.charAt(2) + "b";
		} else {
			return numberString;
		}
	}

	
	public static void leaveParty() {
		Players.getMyPlayer().getLocation().walkTo();
		Mouse.getInstance().click(new Point(610,185));
		Time.sleep(100);
		Menu.sendAction(315, -1, 506, 16035);
		Time.sleep(5);
		Menu.sendAction(646, 254705664, 59, 26229);
		Time.sleep(new SleepCondition() {
			@Override
			public boolean isValid() {
				return Constants.START_ZONE.inTheZone() || Constants.LOBBY.inTheZone();
			}
		}, 1000);
	}
	public static void handlePots() {
		if(Skill.HITPOINTS.getLevel() <= 50) {
			Variables.status = "Attempting to eat...";
    		if(Inventory.getCount(Constants.FOOD1) > 0 && Inventory.getItems(Constants.FOOD1) != null)  {
    			for(Item i : Inventory.getItems(Constants.FOOD1)) {
    				if(i != null) {
    					if(Skill.HITPOINTS.getLevel() <= 50) {
    						Time.sleep(200);
    						Menu.sendAction(74, i.getId()-1, i.getSlot(), 3214);
    						Time.sleep(1550);
    						Skill.HITPOINTS.getLevel();
    					}
    				}
    			}
    		}
    	}
    	if(Skill.PRAYER.getLevel() <= 35) {
			Variables.status = "Attempting to restore...";
    		if(Inventory.getCount(Constants.RESTORES) > 0 && Inventory.getItems(Constants.RESTORES) != null)  {
    			for(Item i : Inventory.getItems(Constants.RESTORES)) {
    				if(i != null) {
    					if(Skill.PRAYER.getLevel() <= 35) {
    						Time.sleep(200);
    						Menu.sendAction(74, i.getId()-1, i.getSlot(), 3214);
    						Time.sleep(1550);
    						Skill.PRAYER.getLevel();
    					}
    				}
    			}
    		}
    	}
	}

	public static boolean isLoggedIn() {
		try {
			Field f = Loader.getClient().getClass().getDeclaredField("bz");
			f.setAccessible(true);
			return f.getBoolean(Loader.getClient());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
