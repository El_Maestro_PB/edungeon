package org.ethan.dungeoneering.edungeon.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Variables {

    public static Image getImage(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch (IOException e) {
            return null;
        }
    }
    
    /**
     * The color of the text
     */

    public static final Color color1 = new Color(255, 255, 255);

    /** 
     * The font that the text in the GUI uses.
     */ 

    public static final Font font1 = new Font("Adobe Garamond Pro Bold", 1, 12);

    /**
     * The GUI image (Still gotta get paint).
     */
   public static Image img1 = getImage("");

    /**
     * The status of the script.
     */
    public static String status = "";
    /**
     * The deaths of the npcs.
     */
    public static int npcDeaths;
    
    /**
     * Did we get items from chest.
     */
	public static boolean chest;
	   /**
     * boss spawned
     */
    public static boolean doBoss = false;
    
    /**
     * The party to join in team dungeoneering.
     */
    public static String joinParty = "";
    
    /**
     * The amount of times the script has died.
     */
    public static int deadCount;
    
    /**
     * The number of floors completed.
     */
    public static int floorCompleted = 0;
    /**
     * Who's who.
     */
    public static boolean former = false, joiner = false;
    /**
     * The number of floors that glitched.
     */
    
    public static int floorsGlitched;
    
    /**
     * Set true if forming the party
     */
    public static boolean formParty;
    /**
     * Name of the saved file
     */
    public static String saveName = "eDungeonSettings.dat";
    
    
    /**
     * Set true if using curses.
     */
    
    public static boolean curses;
    /**
     * Set true if praying.
     */
    
    public static boolean isPraying;
    
    /**
     * Set true once the script can start.
     */
    public static boolean start;
    
    /**
     * Set true once someone has joined your party and the team dungeon can begin.
     */
    public static boolean startDungeon;
    
    /**
     * Set true if the Daconia Rock is found in team dung.
     */
    public static boolean rockFound; 
    /**
     * Used in teamFloor.
     */
    public static boolean dungStart = false;

}
